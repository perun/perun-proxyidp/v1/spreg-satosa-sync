## [2.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v2.1.1...v2.1.2) (2023-10-12)


### Bug Fixes

* post_logout_redirect_uri, non client_id, non flow types ([b8fade0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/b8fade0f7eef9df4a36ad54a38f7ebceff5504e6))

## [2.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v2.1.0...v2.1.1) (2023-07-07)


### Bug Fixes

* update README, show README on pypi.org ([f8c42bb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/f8c42bbbf0f2690f9454218dfb6502d5b617144a))

# [2.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v2.0.1...v2.1.0) (2023-07-07)


### Features

* entry point ([c66182b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/c66182b3a946938acdddba345159003a2a159742))

## [2.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v2.0.0...v2.0.1) (2023-03-09)


### Bug Fixes

* function get_issue_refresh_tokens_value which was causing error ([c0005dc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/c0005dc982da281a1f85af775b30a494d4cc4c2f))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v1.2.0...v2.0.0) (2023-02-15)


### Features

* rpc calls replaced by perun.connector library ([e867228](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/e8672282fcf2d2ef9e22e8c82fbc0534c8140a99))


### BREAKING CHANGES

* new configuration, script is not filtering facilities by rp_type

# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/compare/v1.1.6...v1.2.0) (2022-10-03)


### Features

* redirect URIs with query params support ([5a0c02f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/end-of-life/spreg-satosa-sync/commit/5a0c02f3aa9011399e905085822b2d93cd036f86))

## [1.1.6](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.5...v1.1.6) (2022-03-03)


### Bug Fixes

* correct comparison with True ([5716647](https://github.com/CESNET/spreg-satosa-sync/commit/5716647430e95af33561aa47d68618b972e727fd))

## [1.1.5](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.4...v1.1.5) (2022-02-07)


### Bug Fixes

* **deps:** change imports from Crypto to Cryptodome ([3cadead](https://github.com/CESNET/spreg-satosa-sync/commit/3cadead98b12ddae973552a11ae8db2bfb8de8bf))

## [1.1.4](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.3...v1.1.4) (2021-12-01)


### Bug Fixes

* **deps:** replace pycryptodome with pycryptodomex ([d6f2bb3](https://github.com/CESNET/spreg-satosa-sync/commit/d6f2bb3fc10a7a7a35ea58d0c0aa7b3a2155f726))

## [1.1.3](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.2...v1.1.3) (2021-12-01)


### Bug Fixes

* move dependencies to setup.py ([ee709e2](https://github.com/CESNET/spreg-satosa-sync/commit/ee709e24d49e179dab62d31f1066fa85174b810d))

## [1.1.2](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.1...v1.1.2) (2021-12-01)


### Bug Fixes

* add __init__.py ([b942ed5](https://github.com/CESNET/spreg-satosa-sync/commit/b942ed5ffa0690b5be10de2067bfda612385173d))

## [1.1.1](https://github.com/CESNET/spreg-satosa-sync/compare/v1.1.0...v1.1.1) (2021-12-01)


### Bug Fixes

* move script into a folder ([b84e3d2](https://github.com/CESNET/spreg-satosa-sync/commit/b84e3d2cd42d1e9161fb25d9069b814fe58222e5))

# [1.1.0](https://github.com/CESNET/spreg-satosa-sync/compare/v1.0.0...v1.1.0) (2021-12-01)


### Features

* python script to read clients attributes from perun rpc and write them to mongoDB ([b2c99e4](https://github.com/CESNET/spreg-satosa-sync/commit/b2c99e431b40409b042b6ff1939856964d5164b2))

# 1.0.0 (2021-11-27)

### Bug Fixes

- add setup.py ([8c3b02b](https://github.com/CESNET/spreg-satosa-sync/commit/8c3b02b981c6054c484554742853f62acaebb51a))
